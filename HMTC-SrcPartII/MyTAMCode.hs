module MyTAMCode
where
import TAMCode
import TAMInterpreter

oneton = [
	Label "START",
	-- Get and store input
	GETINT,
	-- Abort if input is less than one
	LOAD (SB(0)),
	LOADL 1,
	LSS,
	JUMPIFNZ "FINISH",
	-- Initial Value
	LOADL 1,
	-- Begin Loop
	Label "LOOP",
	-- Print number
	LOAD (SB(1)),
	PUTINT,
	-- Check if we've hit the limit
	LOAD (SB(0)),
	LOAD (SB(1)),
	EQL,
	JUMPIFNZ "FINISH",
	-- Increment
	LOAD (SB(1)),
	LOADL 1,
	ADD,
	STORE (SB(1)),
	JUMP "LOOP",
	-- END
	Label "FINISH",
	HALT
	]

fac = [
	Label "START",
	-- Get and store input
	GETINT,
	-- Initial Multiplicator and
	 -- default answer
	LOADL 1,
	-- Begin Loop
	Label "LOOP",
	LOAD (SB(0)),
	LOADL 1,
	LSS,
	JUMPIFNZ "FINISH",
	-- Multiply and store
	LOAD (SB(0)),
	LOAD (SB(1)),
	MUL,
	STORE (SB(1)),
	-- Decrement
	LOAD (SB(0)),
	LOADL 1,
	SUB,
	STORE (SB(0)),
	JUMP "LOOP",
	-- END
	Label "FINISH",
	LOAD (SB(1)),
	PUTINT,
	HALT
	]

